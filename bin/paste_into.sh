#!/usr/bin/bash

WMCTRL=/usr/bin/wmctrl
XTE=/usr/bin/xte

${WMCTRL} -i -a $1 && echo "str $2" | ${XTE} -x :0
