#!/bin/bash
# $1 ... top | bottom | left | right
#
# dnf install xprop xwininfo wmctrl
unset x y w h a i

# Let's get our root dimensions and active window id (x y w h a)

eval $(xprop -root | sed -rne 's/^_NET_WORKAREA\(CARDINAL\) = ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+).*$/x=\1;y=\2;w=\3;h=\4/p' \
                          -e 's/^_NET_ACTIVE_WINDOW\(WINDOW\): window id # (0x.*)$/a=\1/p')

# Silliness for only left half because F>24 is dumb
#w=$((w/2))

# Let's get our active window dimensions and save them if we are new

current=$(xwininfo -id ${a} | sed -rne 's/^\s*Width: ([0-9]+)/w=\1/p' -e 's/\s*Height: ([0-9]+)/h=\1/p' -e 's/\s*-geometry [0-9]+x[0-9]+\+([0-9]+)\+([0-9]+)$/x=\1\ny=\2/p')

# Let's remove the height for our bottom panel
((h=h-25))

if [ ! -f /tmp/${a}.txt ]
then
	# This is considered our first time
	echo ${current} > /tmp/${a}.txt
	echo ti=1 >> /tmp/${a}.txt
	echo bi=1 >> /tmp/${a}.txt
	echo li=1 >> /tmp/${a}.txt
	echo ri=1 >> /tmp/${a}.txt
	echo ci=1 >> /tmp/${a}.txt
	echo uli=1 >> /tmp/${a}.txt
	echo lli=1 >> /tmp/${a}.txt
	echo uri=1 >> /tmp/${a}.txt
	echo lri=1 >> /tmp/${a}.txt
fi

# We definitely have a temp file here now
# Let's get our interval
eval $(cat /tmp/${a}.txt | grep 'i=')

[[ -z "$a" ]] && exit 1                            
case "$1" in
  top    ) 
	  case "${ti}" in
		  1 )
			  ((h=h/2))
			  sed -i 's/^ti=1/ti=2/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=w/3))
			  ((h=h/2))
			  ((w=w/3))
			  sed -i 's/^ti=2/ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;
  bottom )
	  case "${bi}" in
		  1 )
	                  ((y=y+(h-(h/2))))
			  ((h=h/2))
			  sed -i 's/^bi=1/bi=2/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=w/3))
			  ((y=y+(h-(h/2))))
			  ((h=h/2))
			  ((w=w/3))
			  sed -i 's/^bi=2/bi=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;
  left   )
	  case "${li}" in
		  1 )
			  ((w=w/2))
			  sed -i 's/^li=1/li=2/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((w=w/3))
			  sed -i 's/^li=2/li=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;
  right  )
	  case "${ri}" in
		  1 )
			  ((x=x+(w-(w/2))))
			  ((w=w/2))
			  sed -i 's/^ri=1/ri=2/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=x+(w-(w/3))))
			  ((w=w/3))
			  sed -i 's/^ri=2/ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  center )
	  case "${ci}" in
		  1 )
			  sed -i 's/^ci=1/ci=2/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=w/3))
			  ((w=w/3))
			  sed -i 's/^ci=2/ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  upperleft )
	  case "${uli}" in
		  1 )
			  ((x=0))
			  ((y=0))
			  ((w=w/2))
			  ((h=h/2))
			  sed -i 's/^uli=1/uli=2/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=0))
			  ((y=0))
			  ((w=w/3))
			  ((h=h/2))
			  sed -i 's/^uli=2/uli=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  lowerleft )
	  case "${lli}" in
		  1 )
			  ((x=0))
	                  ((y=y+(h-(h/2))))
			  ((w=w/2))
			  ((h=h/2))
			  sed -i 's/^lli=1/lli=2/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=0))
	                  ((y=y+(h-(h/2))))
			  ((w=w/3))
			  ((h=h/2))
			  sed -i 's/^lli=2/lli=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  upperright )
	  case "${uri}" in
		  1 )
			  ((x=x+(w-(w/2))))
			  ((y=0))
			  ((w=w/2))
			  ((h=h/2))
			  sed -i 's/^uri=1/uri=2/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=x+(w-(w/3))))
			  ((y=0))
			  ((w=w/3))
			  ((h=h/2))
			  sed -i 's/^uri=2/uri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^lri=./lri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  lowerright )
	  case "${lri}" in
		  1 )
			  ((x=x+(w-(w/2))))
	                  ((y=y+(h-(h/2))))
			  ((w=w/2))
			  ((h=h/2))
			  sed -i 's/^lri=1/lri=2/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  ;;
		  2 )
			  ((x=x+(w-(w/3))))
	                  ((y=y+(h-(h/2))))
			  ((w=w/3))
			  ((h=h/2))
			  sed -i 's/^lri=2/lri=1/' /tmp/${a}.txt
			  sed -i 's/^ci=./ci=1/' /tmp/${a}.txt
			  sed -i 's/^ri=./ri=1/' /tmp/${a}.txt
			  sed -i 's/^ti=./ti=1/' /tmp/${a}.txt
			  sed -i 's/^bi=./bi=1/' /tmp/${a}.txt
			  sed -i 's/^li=./li=1/' /tmp/${a}.txt
			  sed -i 's/^uli=./uli=1/' /tmp/${a}.txt
			  sed -i 's/^lli=./lli=1/' /tmp/${a}.txt
			  sed -i 's/^uri=./uri=1/' /tmp/${a}.txt
			  ;;
	  esac ;;

  original   )
	  eval $( cat /tmp/${a}.txt )
	  rm /tmp/${a}.txt
	  ;;
esac
#echo wmctrl -i -r "$a" -e 0,$x,$y,$w,$h
wmctrl -i -r "$a" -e 0,$x,$y,$w,$h

# Clean up unecessary temp files
for i in /tmp/0x*txt
do
	if [ -f $i ]
	then
		a=$( echo $i | sed 's/^\/tmp\/\(.*\)\.txt/\1/' )
		xwininfo -id $a >/dev/null 2>&1 || rm $i
	fi
done
