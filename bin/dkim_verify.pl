#!/usr/bin/perl
# Copyright (c) 2011, Stephan Chenette and David Saunders
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

use warnings;
use strict;

use Mail::DKIM::Verifier;

my $DEBUG = 1;

my $file = $ARGV[0];

# copy the original email to a file, remove all headers above the DKIM signature 
if ( !defined $file ) {
    print STDERR "Usage: $0 FILE\n";
    exit 1;
}

print "file: " . $file . "\n" if ($DEBUG);

open( my $fd, "<", $file );
my @text = <$fd>;
close $fd;

if( validate_dkim(\@text) ) {
    printf "$ARGV[0] passed DKIM validation\n";
} else {
    printf "$ARGV[0] failed DKIM validation\n";
}

#############################################################################
# Validate DKIM Header
# Returns 1 if valid, otherwise 0
#############################################################################
sub validate_dkim {

    my ($text_arr_ref) = @_;

    my $dkim = Mail::DKIM::Verifier->new();

    my @lines = @{$text_arr_ref};

    my $text = join( q{}, @lines );

    my $dkim_line;
    foreach my $line (@lines) {

        # remove local line terminators
        chomp $line;
        $line =~ s/\015$//;

        if($line =~ /^DKIM/) {
            $dkim_line = $line;
        }

        # use SMTP line terminators
        $dkim->PRINT("$line\015\012");
    }

    $dkim->CLOSE;

    my $result = $dkim->result;

    print $dkim->result_detail."\n" if ($DEBUG || $result ne "pass");

    if($result eq "pass") {
        return 1;
    } else {
        print "$result\n";
    }

    return 0;
}
