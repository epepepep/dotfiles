#!/bin/sh

# to debug, uncomment the following line
#set -x

AWK="/usr/bin/awk"
CP="/bin/cp"
CUT="/bin/cut"
DATE="/bin/date"
DATESTRING="+%Y-%m-%d"
ECHO="/bin/echo"
#EXIFDATE="/usr/local/bin/exif-date"
#EXIFDATE="/home/bepste00/bin/exifdump.py"
#EXIFDATE="/home/bepstein/bin/EXIF.py"
EXIFDATE="/usr/bin/exiftool"
GREP="/bin/grep"
HEAD="/usr/bin/head"
MKDIR="/bin/mkdir"
MKTEMP="/bin/mktemp"
SED="/bin/sed"
TEMPFILE=`${MKTEMP} /tmp/pix.tmp.XXXXXX`
TOUCH="/bin/touch"

# We want the raw time format in the file, so we use UTC as the timezone
TZ="US/Eastern" export TZ

for i in $*
do
	# first we get the date out of the jpg
#	RC1=`${EXIFDATE} $i 2>&1`

	# exifdump.py uses
	#	  DateTimeOriginal(A)='2003:07:27 17:56:38'
	# EXIF.py uses
	#	   EXIF DateTimeOriginal (ASCII): 2004:08:15 01:20:35

#	RC1=`${EXIFDATE} $i 2>&1 | ${GREP} "DateTimeOriginal" | ${CUT} -d"'" -f2 | ${SED} 's/:/-/' | ${SED} 's/:/-/'`
#	RC1=`${EXIFDATE} $i 2>&1 | ${GREP} "EXIF DateTimeOriginal" | ${AWK} '{print $4" "$5;}' | ${SED} 's/:/-/' | ${SED} 's/:/-/'`
	RC1=`${EXIFDATE} $i 2>&1 | ${GREP} "^Create Date" | ${HEAD} -1 | ${AWK} '{print $4" "$5;}' | ${SED} 's/:/-/' | ${SED} 's/:/-/'`

	# next line transfers from GMT to current TZ
#	RC1=`${DATE} -d "${RC1} GMT" "+%Y/%m/%d %H:%M:%S"`

	# Make sure it's an EXIF file, we tell by the return
	RC2=`${ECHO} ${RC1} | grep -ic "not an EXIF file"`
	if [ ${RC2} -gt 0 ]
	then
#		${ECHO} ${RC1}
		continue
	fi

	# Make sure it's not a weird file, we tell by the return
	RC2=`${ECHO} ${RC1} | grep -c "No such device"`
	if [ ${RC2} -gt 0 ]
	then
#		${ECHO} ${RC1}
		continue
	fi

	# Ok, let's snarf out the date from exif
#	DS=`${ECHO} $RC1 | cut -d" " -f1-5`
	DS=${RC1}

	# and let's translate it to [[CC]YY]MMDDhhmm[.ss]
	DS2=`${DATE} -d "${DS}" +%Y%m%d%H%M.%S`

	# let's also translate it to ${DATESTRING} for the directory
	DS3=`${DATE} -d "${DS}" ${DATESTRING}`

	# let's pull the filename out, too
#	FS=`basename \`${ECHO} $RC1 | cut -d" " -f6 \``
	FS=`basename \`${ECHO} $i \``

	# let's make sure our directory exists
	if [ -d ${DS3} ]
	then
		:
	else
		${MKDIR} ${DS3}
	fi

	# ok, time to copy the file in.
#	${CP} -p $i ${DS3}
	${CP} -p $i ${DS3}/${DS2}_${FS}
#	${ECHO} adding ${DS3}/${FS}
	${ECHO} adding ${DS3}/${DS2}_${FS}

	# and let's touch the file to set the correct date
#	${TOUCH} -m -a -t ${DS2} ${DS3}/${FS}
	${TOUCH} -m -a -t ${DS2} ${DS3}/${DS2}_${FS}

	# let's touch the directory for effect
	${TOUCH} -m -a -t ${DS2} ${DS3}

done

# Cleanup
rm ${TEMPFILE}
