#!/bin/bash
unset X Y
eval "$(xdotool getmouselocation -shell 2>/dev/null)"

read outline center fg < <(shuf -n3 /usr/share/X11/rgb.txt | awk '{print $4;}' | xargs )

timeout 1 xeyes     -display :0.${SCREEN} \
        -geometry "100x100+$X+$Y" \
	-outline ${outline} \
	-center ${center} \
	-fg ${fg}
