#!/bin/bash

bits+=(  "X"  "D"  "X" "HR"  "L" "NR" "CC" "ET" )
bits+=(  "X"  "N"  "X" "ID" "WT" "ST"  "X"  "X" )
bits+=( "DP"  "X" "SR" "TD" "ND" "DK" "DR" "PE" )
bits+=( "TA" "NA" "PS"  "X"  "X"  "X"  "X"  "X" )

bit_desc+=( "X" )
bit_desc+=( "(ADS_UF_ACCOUNT_DISABLE, 0x00000002): Specifies that the account is not enabled for authentication." )
bit_desc+=( "X" )
bit_desc+=( "(ADS_UF_HOMEDIR_REQUIRED, 0x00000008): Specifies that the homeDirectory attribute is required." )
bit_desc+=( "(ADS_UF_LOCKOUT, 0x00000010): Specifies that the account is temporarily locked out." )
bit_desc+=( "(ADS_UF_PASSWD_NOTREQD, 0x00000020): Specifies that the password-length policy, as specified in [MS-SAMR] section 3.1.1.8.1, does not apply to this user." )
bit_desc+=( "(ADS_UF_PASSWD_CANT_CHANGE, 0x00000040): Specifies that the user cannot change his or her password." )
bit_desc+=( "(ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED, 0x00000080): Specifies that the cleartext password is to be persisted." )
bit_desc+=( "X" )
bit_desc+=( "(ADS_UF_NORMAL_ACCOUNT, 0x00000200): Specifies that the account is the default account type that represents a typical user." )
bit_desc+=( "X" )
bit_desc+=( "ID (ADS_UF_INTERDOMAIN_TRUST_ACCOUNT, 0x00000800): Specifies that the account is for a domain-to-domain trust." )
bit_desc+=( "(ADS_UF_WORKSTATION_TRUST_ACCOUNT, 0x00001000): Specifies that the account is a computer account for a computer that is a member of this domain." )
bit_desc+=( "(ADS_UF_SERVER_TRUST_ACCOUNT, 0x00002000): Specifies that the account is a computer account for a DC." )
bit_desc+=( "X" )
bit_desc+=( "X" )
bit_desc+=( "(ADS_UF_DONT_EXPIRE_PASSWD, 0x00010000): Specifies that the password does not expire for the account." )
bit_desc+=( "X" )
bit_desc+=( "(ADS_UF_SMARTCARD_REQUIRED, 0x00040000): Specifies that a smart card is required to log in to the account." )
bit_desc+=( "(ADS_UF_TRUSTED_FOR_DELEGATION, 0x00080000): Used by the Kerberos protocol. This bit indicates that the "OK as Delegate" ticket flag, as described in [RFC4120] section 2.8, MUST be set." )
bit_desc+=( "(ADS_UF_NOT_DELEGATED, 0x00100000): Used by the Kerberos protocol. This bit indicates that the ticket-granting tickets (TGTs) of this account and the service tickets obtained by this account are not marked as forwardable or proxiable when the forwardable or proxiable ticket flags are requested. For more information, see [RFC4120]." )
bit_desc+=( "(ADS_UF_USE_DES_KEY_ONLY, 0x00200000): Used by the Kerberos protocol. This bit indicates that only des-cbc-md5 or des-cbc-crc keys, as defined in [RFC3961], are used in the Kerberos protocols for this account." )
bit_desc+=( "(ADS_UF_DONT_REQUIRE_PREAUTH, 0x00400000): Used by the Kerberos protocol. This bit indicates that the account is not required to present valid preauthentication data, as described in [RFC4120] section 7.5.2." )
bit_desc+=( "(ADS_UF_PASSWORD_EXPIRED, 0x00800000): Specifies that the password age on the user has exceeded the maximum password age policy." )
bit_desc+=( "(ADS_UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION, 0x01000000): Used by the Kerberos protocol. When set, this bit indicates that the account (when running as a service) obtains an S4U2self service ticket (as specified in [MS-SFU]) with the forwardable flag set. If this bit is cleared, the forwardable flag is not set in the S4U2self service ticket." )
bit_desc+=( "(ADS_UF_NO_AUTH_DATA_REQUIRED, 0x02000000): Used by the Kerberos protocol. This bit indicates that when the Key Distribution Center (KDC) is issuing a service ticket for this account, the Privilege Attribute Certificate (PAC) MUST NOT be included. For more information, see [RFC4120]." )
bit_desc+=( "(ADS_UF_PARTIAL_SECRETS_ACCOUNT, 0x04000000): Specifies that the account is a computer account for a read-only domain controller (RODC). If this bit is set, the ADS_UF_WORKSTATION_TRUST_ACCOUNT must also be set. This flag is only interpreted by a DC whose DC functional level is DS_BEHAVIOR_WIN2008 or greater." )
bit_desc+=( "X" )
bit_desc+=( "X" )
bit_desc+=( "X" )
bit_desc+=( "X" )
bit_desc+=( "X" )

#######################################
# If argument $1 is empty, ask for it #
#######################################

if [ -z "$1" ]
then
	echo -n "Enter userAccountControl { 2^0 .. 2^31-1 }: "
	read userAccountControl
else
	userAccountControl=$1
fi

#################################################################################
# Verify that $userAccountControl is an integer between 1 and 2^32-1 inclusive. #
#################################################################################

if ! [[ $userAccountControl =~ ^[0-9]+$ ]]
then
	echo $userAccountControl contains non numeric
	exit 1
fi

if [ ! $userAccountControl -gt 0 ]
then
	echo $userAccountControl is not greater than 0
	exit 1
fi

if [ ! $userAccountControl -lt $((2**32)) ]
then
	echo $userAccountControl is not less than $((2**32))
	exit 1
fi

####################################
# Iterate through out bitfield and #
# report what is flagged           #
####################################

for (( i=0; i<$((${#bits[@]}+0)); i++ ))
do
	# userAccountControl & 2^bit = 2^bit
	if (( ($userAccountControl & ((2**$i)) ) == ((2**$i)) ))
	then
		printf "%0.2d: 1 ${bits[$i]} ${bit_desc[$i]}" $i | fmt -w75
	else
		:; #printf "%0.2d: 0\n" $i
	fi
done
